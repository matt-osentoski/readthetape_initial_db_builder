class Stock
  # prices is typically an Array of StockPrices
  attr_accessor :id, :symbol, :name, :description, :sector, :industry, 
  :stock_index, :prices, :active, :state,
  :is_sp500, :is_sp1500, :is_dow_ind

  # The methods below are need to identify the 'symbol' as the hash key
  # for sorts, duplicate checks, etc.
  def eql?(o)
    o.is_a?(Stock) && symbol == o.symbol
  end
  
  def hash
    @symbol.hash
  end

end