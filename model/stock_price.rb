class StockPrice
  attr_accessor :id, :symbol, :volume, :opening_price, :closing_price,
    :adj_closing_price,
    :low_price, :high_price, :price_date, :period
end