
# This script builds a SQLite database with random stocks from a MySQL database
require 'sqlite3'

require './dao/stock_dao'

DB_PATH = '/tmp/trading_db.db'
MAX_STOCK_SYMBOLS = 50

DROP_METADATA_TABLE = 'DROP TABLE IF EXISTS android_metadata';
CREATE_METADATA_TABLE = "CREATE TABLE android_metadata(locale TEXT DEFAULT 'en_US');"
POPULATE_METADATA_TABLE = "INSERT INTO android_metadata VALUES ('en_US');"

DROP_STOCKS_TABLE = 'DROP TABLE IF EXISTS stocks;'
CREATE_STOCKS_TABLE = 'CREATE TABLE stocks ( _id INTEGER PRIMARY KEY AUTOINCREMENT, 
        symbol TEXT NOT NULL COLLATE NOCASE, downloaded INTEGER);'
        
DROP_STOCK_PRICES_TABLE = 'DROP TABLE IF EXISTS stock_prices;'        
CREATE_STOCK_PRICES_TABLE = 'CREATE TABLE stock_prices ( _id INTEGER PRIMARY KEY AUTOINCREMENT, 
        symbol TEXT NOT NULL COLLATE NOCASE, period TEXT NOT NULL, closing_price REAL, high_price REAL, volume INTEGER, price_date TEXT, 
        opening_price REAL, low_price REAL, adj_closing_price REAL);'

CREATE_STOCK_PRICES_INDEX = 'CREATE INDEX stock_prices_symbol ON stock_prices (symbol);'

db = SQLite3::Database.new(DB_PATH)

puts "Rebuilding the database..."
# Rebuild the Database
db.execute(DROP_METADATA_TABLE)
db.execute(DROP_STOCKS_TABLE)
db.execute(DROP_STOCK_PRICES_TABLE)
db.execute(CREATE_STOCKS_TABLE)
db.execute(CREATE_STOCK_PRICES_TABLE)
db.execute(CREATE_STOCK_PRICES_INDEX)
db.execute(CREATE_METADATA_TABLE)
db.execute(POPULATE_METADATA_TABLE)

puts "Gathering a list of stocks (this may take a few minutes)..."
dao = StockDAO.new()
stock_symbols = dao.get_stock_symbols_by_price(5000, 5, 100)  # Grab first 5000 stocks between $5 - $100

puts "Filling the stocks tables"
db.transaction
for symbol in stock_symbols
  db.execute("INSERT INTO stocks ('symbol', 'downloaded') VALUES (?, ?)", symbol.encode('utf-8'), 0)
end
db.commit

selected_symbols = stock_symbols.sample(MAX_STOCK_SYMBOLS)

for symbol in selected_symbols
  puts "Grabbing prices for: " + symbol
  stock_prices = dao.get_stock_prices(symbol)
  db.transaction
  db.execute("UPDATE stocks SET downloaded = ? WHERE symbol = ?", 1, symbol.encode('utf-8'))
  for sp in stock_prices
    db.execute("INSERT INTO stock_prices ('symbol', 'period', 'closing_price', 'high_price', 'volume', 
      'price_date', 'opening_price', 'low_price', 'adj_closing_price') 
      VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", sp.symbol.encode('utf-8'), sp.period.encode('utf-8'), sp.closing_price, 
      sp.high_price, sp.volume,
      sp.price_date.to_s.encode('utf-8'), sp.opening_price, sp.low_price, sp.adj_closing_price)
  end
  db.commit
end
puts "Finished!"




