require 'rubygems' 
require 'mysql'

require './model/stock'
require './model/stock_price'

class StockDAO
  
  DB_HOST = '127.0.0.1'
  DB_USERNAME = 'root'
  DB_PASSWORD = 'password'
  DB_DATABASE = 'stocks_dev'
  
  def conn
    return Mysql.real_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, 
      DB_DATABASE)
  end
  
    # Query the database to retrieve a stock object
  def get_stock(symbol)
    
    stock = Stock.new()
    begin
      dbh = self.conn()
      stmt = dbh.prepare("SELECT id, symbol, name, description, sector, industry, stock_index, state, active FROM stocks WHERE symbol = ?")
      rs = stmt.execute(symbol)
      
      if rs == nil
        return nil
      end
    
      rs.each do |row|
        stock.id = row[0]
        stock.symbol = row[1]
        stock.name = row[2]
        stock.description = row[3]
        stock.sector = row[4]
        stock.industry = row[5]
        stock.stock_index = row[6]
        stock.state = row[7]
        stock.active = row[8]
      end
    rescue MysqlError => e
      print "Error code: ", e.errno, "\n"
      print "Error message: ", e.error, "\n"
    ensure
      # disconnect from server
      if dbh != nil
        dbh.close
      end
    end # begin / rescue block
    return stock
  end # get_stock()
  
  # Returns a Stock object loaded with historical prices
  # @param symbol The stock symbol
  # @param period The tick value for this price (Ex: 'd' for day, 'w' for week, 'm' for month)
  # @return an array of stock_price objects
  def get_stock_prices(symbol, period = 'd')

    stock_prices = []
    
    # Query the Database and return a number of prices
    begin
      dbh = self.conn()
      stmt = dbh.prepare("SELECT id, symbol, opening_price, closing_price, adj_closing_price,
        low_price, high_price, volume, price_date, period
        FROM stock_prices WHERE symbol = ? AND period = ? ORDER BY price_date")
      rs = stmt.execute(symbol, period)
    
      rs.each do |row|
        stock_price = StockPrice.new()
        stock_price.id = row[0]
        stock_price.symbol = row[1]
        stock_price.opening_price = row[2].to_f
        stock_price.closing_price = row[3].to_f
        stock_price.adj_closing_price = row[4].to_f
        stock_price.low_price = row[5].to_f
        stock_price.high_price = row[6].to_f
        stock_price.volume = row[7].to_i
        stock_price.price_date = row[8]
        stock_price.period = row[9]
        
        stock_prices << stock_price
      end
    rescue MysqlError => e
      print "Error code: ", e.errno, "\n"
      print "Error message: ", e.error, "\n"
    ensure
      # disconnect from server
      if dbh != nil
        dbh.close
      end
    end # begin / rescue block
    return stock_prices
  end 
  
  
  # Return a list of stocks within a price range
  # @param count Maximum number of random stocks to gather
  def get_stock_symbols_by_price(count = 500, min_price = 5, max_price = 100)
    symbols = []
    begin
      dbh = self.conn()
      # NOTE, using a 'LIMIT' with a 'DISTINCT' was proving problematic,
      # so, I'm simply grabbing a random order and grabbing the top values
      # as a limit.
      stmt = dbh.prepare('SELECT DISTINCT sp.symbol FROM stock_prices sp
        WHERE EXISTS (
          SELECT 1 FROM stock_prices WHERE symbol = sp.symbol
          AND closing_price > ? AND closing_price < ?
        )')
      rs = stmt.execute(min_price, max_price)
    
      rs.each do |row|
        symbol = row[0]
        symbols << symbol
      end
      
    rescue MysqlError => e
      print "Error code: ", e.errno, "\n"
      print "Error message: ", e.error, "\n"
    ensure
      # disconnect from server
      if dbh != nil
        dbh.close
      end
    end # begin / rescue block
    return symbols.first(count) # Use 'first()' instead of a 'LIMIT' in the query
  end
  
  
  # Return a list of random stocks
  # @param count Maximum number of random stocks to gather
  def get_random_stock_symbols(count = 50, min_price = 5, max_price = 100)
    symbols = []
    begin
      dbh = self.conn()
      # NOTE, using a 'LIMIT' with a 'DISTINCT' was proving problematic,
      # so, I'm simply grabbing a random order and grabbing the top values
      # as a limit.
      stmt = dbh.prepare('SELECT DISTINCT sp.symbol FROM stock_prices sp
        WHERE EXISTS (
          SELECT 1 FROM stock_prices WHERE symbol = sp.symbol
          AND closing_price > ? AND closing_price < ?
        )
        ORDER BY RAND()')
      rs = stmt.execute(min_price, max_price)
    
      rs.each do |row|
        symbol = row[0]
        symbols << symbol
      end
      
    rescue MysqlError => e
      print "Error code: ", e.errno, "\n"
      print "Error message: ", e.error, "\n"
    ensure
      # disconnect from server
      if dbh != nil
        dbh.close
      end
    end # begin / rescue block
    return symbols.first(count) # Use 'first()' instead of a 'LIMIT' in the query
  end
end