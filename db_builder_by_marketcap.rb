# This script builds a SQLite database with the top market cap stocks from the S&P 500
# The top stocks are determined by a CSV file (living in the root of this script)
require 'rubygems'
require 'sqlite3'
require 'csv'  # Ruby 1.9's CSV engine

require './dao/stock_dao'

DB_PATH = '/tmp/trading_db.db'
SP500_CSV_FILE = 'sp500_by_market_cap.csv'
MAX_STOCK_SYMBOLS = 50

DROP_METADATA_TABLE = 'DROP TABLE IF EXISTS android_metadata';
CREATE_METADATA_TABLE = "CREATE TABLE android_metadata(locale TEXT DEFAULT 'en_US');"
POPULATE_METADATA_TABLE = "INSERT INTO android_metadata VALUES ('en_US');"

DROP_STOCKS_TABLE = 'DROP TABLE IF EXISTS stocks;'
CREATE_STOCKS_TABLE = 'CREATE TABLE stocks ( _id INTEGER PRIMARY KEY AUTOINCREMENT, 
        symbol TEXT NOT NULL COLLATE NOCASE, downloaded INTEGER);'
        
DROP_STOCK_PRICES_TABLE = 'DROP TABLE IF EXISTS stock_prices;'        
CREATE_STOCK_PRICES_TABLE = 'CREATE TABLE stock_prices ( _id INTEGER PRIMARY KEY AUTOINCREMENT, 
        symbol TEXT NOT NULL COLLATE NOCASE, period TEXT NOT NULL, closing_price REAL, high_price REAL, volume INTEGER, price_date TEXT, 
        opening_price REAL, low_price REAL, adj_closing_price REAL);'

CREATE_STOCK_PRICES_INDEX = 'CREATE INDEX stock_prices_symbol ON stock_prices (symbol);'

db = SQLite3::Database.new(DB_PATH)

puts "Rebuilding the database..."
# Rebuild the Database
db.execute(DROP_METADATA_TABLE)
db.execute(DROP_STOCKS_TABLE)
db.execute(DROP_STOCK_PRICES_TABLE)
db.execute(CREATE_STOCKS_TABLE)
db.execute(CREATE_STOCK_PRICES_TABLE)
db.execute(CREATE_STOCK_PRICES_INDEX)
db.execute(CREATE_METADATA_TABLE)
db.execute(POPULATE_METADATA_TABLE)

dao = StockDAO.new()

puts "Gathering a list of the top S&P 500 stocks (this may take a few minutes)..."
stock_symbols = []
# Grab the top stocks from the S&P 500 (by market cap)
csv_text = File.read(SP500_CSV_FILE)
csv = CSV.parse(csv_text, :headers => true)
csv.each do |row|
  # Skip Berkshire Hathaway, due to huge share price
  if row[1] == 'BRK-A'
    next
  end
  stock = dao.get_stock(row[1])
  if stock != nil
    puts "Found " + row[1]
    stock_symbols << row[1]
  else
    puts row[1] + " is missing in the DB"
  end
end
#stock_symbols = dao.get_stock_symbols_by_price(5000, 5, 100)  # Grab first 5000 stocks between $5 - $100

puts "Filling the stocks tables"
db.transaction
for symbol in stock_symbols
  db.execute("INSERT INTO stocks ('symbol', 'downloaded') VALUES (?, ?)", symbol.encode('utf-8'), 0)
end
db.commit

stock_symbols.each_with_index do |symbol, idx|
  if idx == MAX_STOCK_SYMBOLS
    break
  end
  puts "Grabbing prices for: " + symbol
  stock_prices = dao.get_stock_prices(symbol)
  db.transaction
  db.execute("UPDATE stocks SET downloaded = ? WHERE symbol = ?", 1, symbol.encode('utf-8'))
  for sp in stock_prices
    db.execute("INSERT INTO stock_prices ('symbol', 'period', 'closing_price', 'high_price', 'volume', 
      'price_date', 'opening_price', 'low_price', 'adj_closing_price') 
      VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", sp.symbol.encode('utf-8'), sp.period.encode('utf-8'), sp.closing_price, 
      sp.high_price, sp.volume,
      sp.price_date.to_s.encode('utf-8'), sp.opening_price, sp.low_price, sp.adj_closing_price)
  end
  db.commit
end
puts "Finished!"




